CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Feeds Fetcher Post module allows users to use POST parameters in Feeds
requests.

 * For a full description of the module visit:
   https://www.drupal.org/project/feeds_fetcher_post

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/feeds_fetcher_post


REQUIREMENTS
------------

This module requires feeds module.


INSTALLATION
------------

Install the Feeds Fetcher Post module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Feeds Fetcher Post
       module.
    2. Navigate to Administration > Structure > Feed Types > your feeder type to
       edit settings.
    3. Select "Download from URL additional POST parameters" as fetcher.
    4. Navigate to Administration > Content > Feeds > your feed.
    5. Enter URL and post parameters.


TROUBLESHOOTING
---------------


FAQ
---



MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
