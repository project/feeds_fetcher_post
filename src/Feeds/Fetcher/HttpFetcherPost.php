<?php

namespace Drupal\feeds_fetcher_post\Feeds\Fetcher;

use Drupal\feeds\Exception\FetchException;
use Drupal\feeds\Feeds\Fetcher\HttpFetcher;
use Drupal\feeds\Utility\Feed;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

/**
 * Defines an HTTP Post fetcher.
 *
 * @FeedsFetcher(
 *   id = "httpfetcherpost",
 *   title = @Translation("Download from URL additional POST parameters"),
 *   description = @Translation("Downloads data from a URL using Drupal's HTTP request handler with additional POST parameters."),
 *   form = {
 *     "configuration" = "Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherForm",
 *     "feed" = "Drupal\feeds_fetcher_post\Feeds\Fetcher\Form\HttpFetcherPostFeedForm",
 *   }
 * )
 */
class HttpFetcherPost extends HTTPFetcher {

  /**
   * Performs a request.
   *
   * @param string $url
   *   The URL to POST.
   * @param string $sink
   *   The location where the downloaded content will be saved. This can be a
   *   resource, path or a StreamInterface object.
   * @param string|false $cache_key
   *   (optional) The cache key to find cached headers. Defaults to false.
   * @param array $options
   *   (optional) Additional options to pass to the request.
   *   See https://docs.guzzlephp.org/en/stable/request-options.html.
   * @param array $feed_configuration
   *   (optional) Feed entity configuration.
   *
   * @return mixed
   *   A Guzzle response.
   *
   * @throws \Drupal\feeds\Exception\FetchException
   *   Thrown if the GET request failed.
   *
   * @see \GuzzleHttp\RequestOptions
   */
  protected function get($url, $sink, $cache_key = FALSE, array $options = [], array $feed_configuration = []) {
    $url = Feed::translateSchemes($url);

    $options += [
      RequestOptions::SINK => $sink,
      RequestOptions::TIMEOUT => $this->configuration['request_timeout'],
      RequestOptions::HEADERS => [],
    ];

    if ($feed_configuration['headers'] !== '') {
      foreach (explode("\r\n", $feed_configuration['headers']) as $row) {
        if (preg_match('/(.*?): (.*)/', $row, $matches)) {
          $options[RequestOptions::HEADERS][$matches[1]] = $matches[2];
        }
      }
    }
    if ($feed_configuration['post_parameter'] !== '') {
      if ($feed_configuration['post_variant'] === 'BODY') {
        $options[RequestOptions::BODY] = $feed_configuration['post_parameter'];
      }
      else {
        foreach (explode("\r\n", $feed_configuration['post_parameter']) as $row) {
          if (preg_match('/(.*?): (.*)/', $row, $matches)) {
            $options[RequestOptions::FORM_PARAMS][$matches[1]] = $matches[2];
          }
        }
      }
    }

    // Add cached headers if requested.
    if ($cache_key && ($cache = $this->cache->get($cache_key))) {
      if (isset($cache->data['etag'])) {
        $options[RequestOptions::HEADERS]['If-None-Match'] = $cache->data['etag'];
      }
      if (isset($cache->data['last-modified'])) {
        $options[RequestOptions::HEADERS]['If-Modified-Since'] = $cache->data['last-modified'];
      }
    }

    try {
      $response = $this->client->requestAsync('POST', $url, $options)->wait();
    }
    catch (RequestException $e) {
      $args = [
        '%site' => $url,
        '%error' => $e->getMessage(),
        '%headers' => $feed_configuration['headers'],
        '%post_parameter' => $feed_configuration['post_parameter'],
        '%post_variant' => $feed_configuration['post_variant'],
      ];
      // Since the fetch is getting aborted, delete the downloaded file.
      $this->fileSystem->unlink($sink);
      throw new FetchException(strtr('The feed from %site seems to be broken because of error "%error". %post_variant %headers %post_parameter', $args));
    }

    if ($cache_key !== '') {
      $this->cache->set($cache_key, array_change_key_case($response->getHeaders()));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration += [
      'headers' => '',
      'post_parameter' => '',
      'post_variant' => 'FORM_PARAMS',
    ];
    return $default_configuration;
  }

}
