<?php

namespace Drupal\feeds_fetcher_post\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherFeedForm;

/**
 * Provides a form on the feed edit page for the HttpFetcher.
 */
class HttpFetcherPostFeedForm extends HttpFetcherFeedForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, ?FeedInterface $feed = NULL) {
    parent::buildConfigurationForm($form, $form_state, $feed);

    $form = parent::buildConfigurationForm($form, $form_state, $feed);
    $form['headers'] = [
      '#title' => $this->t('Headers'),
      '#description' => $this->t("List of Header values, one pair per line: KEY: VALUE"),
      '#type' => 'textarea',
      '#default_value' => $feed->getConfigurationFor($this->plugin)['headers'],
    ];
    $form['post_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('POST request'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['post_fieldset']['post_parameter'] = [
      '#title' => $this->t('POST parameters'),
      '#type' => 'textarea',
      '#default_value' => $feed->getConfigurationFor($this->plugin)['post_parameter'],
    ];
    $form['post_fieldset']['post_variant'] = [
      '#title' => $this->t('POST variant'),
      '#type' => 'radios',
      '#options' => [
        'FORM_PARAMS' => $this->t('List of POST values, one pair per line: KEY: VALUE (FORM_PARAMS)'),
        'BODY' => $this->t('Send textfield content as it is (BODY)'),
      ],
      '#default_value' => $feed->getConfigurationFor($this->plugin)['post_variant'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ?FeedInterface $feed = NULL) {
    parent::submitConfigurationForm($form, $form_state, $feed);
    $feed_config = $feed->getConfigurationFor($this->plugin);
    $feed_config['headers'] = $form_state->getValue('headers');
    $feed_config['post_parameter'] = $form_state->getValue([
      'post_fieldset',
      'post_parameter',
    ]);
    $feed_config['post_variant'] = $form_state->getValue([
      'post_fieldset',
      'post_variant',
    ]);
    $feed->setConfigurationFor($this->plugin, $feed_config);
  }

}
